# README #

## What is this repository for? ##

### Monte Carlo Tree Search ###

* This package contains a CPU parallelized version of Monte Carlo Tree Search with a simple path planning for coverage example.

## How do I get set up? ##

### Monte Carlo Tree Search ###

* Install the map_server package from ROS:

sudo apt-get install ros-<rosversion>-map-server

* This repo is organized as a ROS workspace. After you've cloned the repo, make the package (using catkin_make in the release directory). After sourcing the workspace (source release/devel/setup.bash) you should be able to launch the test case with:

roslaunch mcts multi_robot_sim.launch

While this example uses ROS, the MonteCarloGrove, MonteCarloTree, and MonteCarloTreeNode classes don't use ROS (except for the OccupancyGrid and Path message types). If you change how maps and paths are represented, this could be unmarried from ROS.

## Who do I talk to? ##

### Monte Carlo Tree Search ###

* phyatt16@gmail.com
